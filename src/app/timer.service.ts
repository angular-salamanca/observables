import { Injectable } from '@angular/core';
import { interval } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TimerService {

  public timer$ = interval(500).pipe(
    filter(numero => numero % 2 === 0),
    map(numero => numero * numero)
  );

  constructor() { }
}
