import { Component, OnInit, OnDestroy } from '@angular/core';
import { MensajeroService, Temas } from './mensajero.service';
import { TimerService } from './timer.service';
import { filter, map, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  public numero;
  private suscripcionTimer: Subscription;
  private suscripcionMsj: Subscription;

  constructor(private timerService: TimerService, private mensajeroService: MensajeroService) { }

  ngOnInit() {
    this.suscripcionTimer = this.timerService.timer$.pipe(
      filter(n => n > 50),
      map(n => `el número ${n}`)
    ).subscribe(
      n => this.numero = n
    );
    this.suscripcionMsj = this.mensajeroService.mensajes.pipe(
      filter(mensaje => mensaje.tema === Temas.facturas),
      take(2)
    ).subscribe(
      mensaje => console.log('Componente App recibe: ', mensaje)
    );
  }

  ngOnDestroy() {
    this.suscripcionMsj.unsubscribe();
    this.suscripcionTimer.unsubscribe();
  }
}
