import { Component, OnInit, OnDestroy } from '@angular/core';
import { TimerService } from '../timer.service';
import { MensajeroService, Temas } from '../mensajero.service';
import { filter, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit, OnDestroy {

  public numero: number;
  private suscripcionTimer: Subscription;
  private suscripcionMsj: Subscription;

  constructor(private timerService: TimerService, private mensajeroService: MensajeroService) { }

  ngOnInit() {
    this.suscripcionTimer = this.timerService.timer$.subscribe(
      n => this.numero = n
    );
    this.suscripcionMsj = this.mensajeroService.mensajes.pipe(
      filter(mensaje => mensaje.tema === Temas.usuarios)
    ).subscribe(
      mensaje => console.log('Componente Cabecera recibe:', mensaje)
    );
  }

  ngOnDestroy() {
    this.suscripcionTimer.unsubscribe();
    this.suscripcionMsj.unsubscribe();
  }

}
