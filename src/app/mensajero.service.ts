import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

export enum Temas { usuarios, facturas }

export interface Mensaje {
  tema: Temas;
  texto: string;
}

@Injectable({
  providedIn: 'root'
})
export class MensajeroService {

  private mensajero = new BehaviorSubject<Mensaje>({ tema: null, texto: null });
  private mensajero$ = this.mensajero.asObservable().pipe(
    filter(mensaje => mensaje.tema !== null)
  );

  constructor() {
    setTimeout(() => this.emitir({ tema: Temas.usuarios, texto: 'Usuarios cargados' }), 3000);
    setTimeout(() => this.emitir({ tema: Temas.facturas, texto: 'Facturas cargadas' }), 4500);
    setTimeout(() => this.emitir({ tema: Temas.usuarios, texto: 'Nuevo usuario' }), 6500);
    setTimeout(() => this.emitir({ tema: Temas.usuarios, texto: 'Usuario modificado' }), 7500);
    setTimeout(() => this.emitir({ tema: Temas.facturas, texto: 'Factura modificada' }), 10000);
    setTimeout(() => this.mensajero.complete(), 10100);
  }

  public emitir(mensaje: Mensaje) {
    this.mensajero.next(mensaje);
  }

  get mensajes() {
    return this.mensajero$;
  }
}
